var express = require("express"),
    handlebars = require('express-handlebars');
 
var app = express();
 
 
var posts = [{
                "subject": "Post numero 1",
                "description": "Descripcion 1",
                "time": new Date()
            }];
 
app.engine('handlebars', handlebars({defaultLayout: 'main' }));
app.set('view engine', 'handlebars');
app.set("views", "./views");
 
 
app.get('/posts', function(req, res){
    res.render('posts', { "title": "Hola gente", "posts" : JSON.stringify(posts) } );
});
 
app.listen(8080);

